require_relative 'lib/tchae/version'

D = 'Having to validate method parameters and/or the result of method is a
quite common and repetitive task. Tchae is an attempt to capture this task in a small yet generic reuseable library. Tchae provides validated variants of the standard define_method ruby methods , that will create the same method as the standard would create wrapped into validation code.'.freeze
F = ['lib/tchae.rb'].freeze
F += Dir.glob('lib/*.rb') + Dir.glob('lib/tchae/*.rb') 

Gem::Specification.new do |s|
  s.name        = 'tchae'
  s.version     = Tchae::VERSION
  s.summary     = 'Tchae is a lightweight ruby validation library'
  s.description = D
  s.authors     = ['D.M.']
  s.email       = 'dev@aithscel.eu'
  s.files       = F
  s.required_ruby_version = '>= 2.4.0'
  s.add_development_dependency 'rake', '~> 12.3'
  s.add_development_dependency 'rubocop', '~> 0.51'
  s.homepage = 'https://gitlab.com/dm0da/tchae'
  s.license = 'MIT'
  s.metadata = {
    "bug_tracker_uri" => "https://gitlab.com/dm0da/tchae/issues",
    "changelog_uri" => "https://gitlab.com/dm0da/tchae/blob/master/CHANGELOG",
    "source_code_uri" => "https://gitlab.com/dm0da/tchae/tree/master",
    "wiki_uri" => "https://gitlab.com/dm0da/tchae/wikis/home"
  }
end
