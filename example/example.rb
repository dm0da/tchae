#!/usr/bin/env ruby

require 'tchae'

class Something < String

  @validator = Tchae.Wrapper.args.positional.expect do 
    arg { is_a? Integer }
  end
  
  create_validated_method(:repeat, @validator) do |count|
    result = String.new
    count.times { result << self } 
    result 
  end
end

s = Something.new('Hello')

s.repeat(5)  # ==> "HelloHelloHelloHelloHello" 

s.repeat('five')  # ==> Tea::ArgumentTypeException 


