require_relative './test_helper'

Main = self

module TM_TM
end

class TC_Basic  < Test::Unit::TestCase
  # check that version is defined and accessible
  def test_version
    assert_nothing_raised do Tchae::VERSION end
  end

  def test_callable
    assert self.class.respond_to? :create_validated_method
    assert Main.class.respond_to? :create_validated_method
    assert TM_TM.respond_to? :create_validated_method
  end
  
  def test_argument_error
    assert_raise(ArgumentError) do 
      TM_TM.create_validated_method(:test, lambda: ->{}){ }
    end
    assert_nothing_raised  do 
      TM_TM.create_validated_method(:test1, lambda: ->{})
    end
    
    assert_nothing_raised  do 
      TM_TM.create_validated_method(:test2){ }
    end
  end
  
end


class TC_SingletonBasic  < Test::Unit::TestCase
  def test_callable
    assert self.class.respond_to? :create_validated_singleton_method
    assert Main.class.respond_to? :create_validated_singleton_method
    assert TM_TM.respond_to? :create_validated_singleton_method
  end
  
  def test_argument_error
    assert_raise(ArgumentError) do 
      TM_TM.create_validated_singleton_method(:test, lambda: ->{}){ }
    end
    assert_nothing_raised  do 
      TM_TM.create_validated_singleton_method(:test1, lambda: ->{})
    end
    
    assert_nothing_raised  do 
      TM_TM.create_validated_singleton_method(:test2){ }
    end
  end
  
end

