require_relative './test_helper'
require 'date'



module Y6_Singleton
  include Y6
  def CR6_Singleton
  # instance method mia that receives an argument without validation
    create_validated_singleton_method(:mia){ |nval| @ivar = nval }
    create_validated_singleton_method(:kia){ | nval: | @ivar = nval }
    
  # instance method miav that receives an argument with Validation 
  # is_a?(Integer)
    create_validated_singleton_method(:miav, w1){ |nval| @ivar = nval}
    create_validated_singleton_method(:qiav, q1){ |nval| @ivar = nval}
    create_validated_singleton_method(:kiav, k1){ |nval: | @ivar = nval}
    
  # instance method miaw that receives two arguments with Validation 
  #   is_a?(Integer) and class_respond_to?(:now)
    create_validated_singleton_method(:miaw, w2){ |v1,v2| @i1 = v1 ;   @i2 = v2  }
    create_validated_singleton_method(:qiaw, q2){ |v1,v2| @i1 = v1 ;   @i2 = v2  }
    create_validated_singleton_method(:kiaw, k2){ |v1: , v2: | @i1 = v1 ;   @i2 = v2  }
    
  # same as miaw with a wrong default value for second optional param 
    create_validated_singleton_method(:m1aw, w2) { |v1, v2='x2'| @i1 = v1; @i2 = v2 }
    create_validated_singleton_method(:q1aw, q2) { |v1, v2='x2'| @i1 = v1; @i2 = v2 } 
    create_validated_singleton_method(:k1aw, k2) { |v1: , v2: 'x2'| @i1 = v1; @i2 = v2 }
    
  # Validating method result
  # in: Integer, out: String
    create_validated_singleton_method(:r1, w3){ |i|  i.to_s  }
    create_validated_singleton_method(:rq1, q3){ |i|  i.to_s  }
    
    # simulate a programing error
  
    create_validated_singleton_method(:rwrong, w4){|i,s| if false then s else i end }
    create_validated_singleton_method(:rqwrong, q4){|i,s| if false then s else i end }
  
  end
  
  def LR6_Singleton
  # instance method mia that receives an argument without validation
    create_validated_singleton_method :mia, lambda: ->(nval){ @ivar = nval }
    create_validated_singleton_method :kia, lambda: ->(nval: ){ @ivar = nval }
     
  # instance method miav that receives an argument with Validation 
  # is_a?(Integer)
    create_validated_singleton_method :miav, w1, lambda: ->(nval){  @ivar = nval}
    create_validated_singleton_method :qiav, q1, lambda: ->(nval){  @ivar = nval}
    create_validated_singleton_method :kiav, k1, lambda: ->(nval: ){  @ivar = nval}
  
  # instance method miaw that receives two arguments with Validation 
  #   is_a?(Integer) and class_respond_to?(:now)
    create_validated_singleton_method :miaw, w2, lambda: ->(v1,v2){ 
      @i1 = v1 ;   @i2 = v2   
    }
    create_validated_singleton_method :qiaw, q2, lambda: ->(v1,v2){ 
      @i1 = v1 ;   @i2 = v2   
    }
    create_validated_singleton_method :kiaw, k2, lambda: ->(v1:, v2:){ 
      @i1 = v1 ;   @i2 = v2   
    }
  # same as miaw with a wrong default value for second optional param 
    create_validated_singleton_method :m1aw, w2, lambda: ->(v1,v2='x2'){ 
      @i1 = v1; @i2 = v2  
    }
    create_validated_singleton_method :q1aw, q2, lambda: ->(v1,v2='x2'){ 
      @i1 = v1; @i2 = v2  
    } 
    create_validated_singleton_method :k1aw, k2, lambda: ->(v1: , v2: 'x2'){ 
      @i1 = v1; @i2 = v2  
    } 
  
  # Validating method result
  # in: Integer, out: String
    create_validated_singleton_method :r1, w3, lambda: ->(i){  i.to_s  }
    create_validated_singleton_method :rq1, q3, lambda: ->(i){  i.to_s  }
    
    # simulate a programing error  
    create_validated_singleton_method :rwrong, w4, lambda: ->(i,s){
     if false then s else i end 
    }
    create_validated_singleton_method :rqwrong, q4, lambda: ->(i,s){
     if false then s else i end 
    }
  
  end
end

class C6_4_SingletonTest
  
  include X6
  include W6_EX
  include Y6_Singleton
  
  
end

class C6L_4_SingletonTest

  include X6
  include W6_EX
  include Y6_Singleton
  
  
end

class E6_4_SingletonTest
  
  include X6
  include W6_RETWRA
  include Y6_Singleton
  
  
end

class E6L_4_SingletonTest

  include X6
  include W6_RETWRA
  include Y6_Singleton
    
end



class O6_4_SingletonTest
  
  include X6
  include W6_RETOR
  include Y6_Singleton
  
  
end

class O6L_4_SingletonTest

  include X6
  include W6_RETOR
  include Y6_Singleton
    
end
 

module TM_Singleton_Validate_Result
  def test_result_ok
    assert_valid_equal('1') do @c1.r1(1)  end
    assert_invalid(:ArgumentType) do   @c1.r1('1')   end
    assert_valid_equal('1') do @c1.rq1(1)  end
    assert_invalid(:ArgumentType) do   @c1.rq1('1')   end
  end
  
  
  def test_result_notok
    assert_invalid(:ReturnType) do  @c1.rwrong(1,'2')   end
    assert_invalid(:ReturnType) do  @c1.rqwrong(1,'2')   end
  end

end

module TM_Singleton_Validate_Args_Noerr
# note: these tests are with methods created with a Nil method wrapper
#       thus having the RAISE invalid-handling per default,
#       thus they must be tested with the RAISE asserts only
  def test_defv_ivar_wo_validate_arg
    assert_nil @c1.ivar
    assert_valid_equal(true) do @c1.mia(true) end
    assert @c1.ivar

    assert_valid_equal(false) do @c1.mia(false) end
    assert_false @c1.ivar

  end
  
  def test_kw_ivar_wo_validate_arg
    assert_nil @c1.ivar
    assert_valid_equal(true) do @c1.kia(nval: true) end
    assert @c1.ivar

    assert_false @c1.kia(nval: false)
    assert_false @c1.ivar

    
  end
end

module TM_Singleton_Validate_Args

  def test_defv_ivar_with_validate_arg
    assert_nil @c1.ivar
    assert_invalid(:ArgumentType) do @c1.miav(true) end
    assert_nil @c1.ivar

    assert_valid_equal(2) do @c1.miav(2) end
    assert_equal 2, @c1.ivar

    
  end
  def test_q_ivar_with_validate_arg
    assert_nil @c1.ivar
    assert_invalid(:ArgumentType) do @c1.qiav(true) end
    assert_nil @c1.ivar
    
    @c1.qiav(2)
    
    assert_valid_equal(2) do @c1.qiav(2) end
    assert_equal 2, @c1.ivar
   
  end

  def test_kw_ivar_with_validate_arg
    assert_nil @c1.ivar
#    assert_raise(Tchae::ArgumentTypeException) do @c1.kiav(nval: true) end
    assert_invalid(:ArgumentType) do @c1.kiav(nval: true) end
    assert_nil @c1.ivar

    assert_valid_equal(2) do @c1.kiav(nval: 2) end
    assert_equal 2, @c1.ivar

    
  end


  def test_defv_multivar_with_validate_arg
    assert_nil @c1.i1
    assert_nil @c1.i2
    
    # a valid value for second param
    v2=Time.now
    
    # pass a wrong first param
    assert_invalid(:ArgumentType)  do @c1.miaw(true, v2) end
    assert_nil @c1.i1
    assert_nil @c1.i2

    # pass a wrong second param
    assert_invalid(:ArgumentType)  do @c1.miaw(4, 4) end
    assert_nil @c1.i1
    assert_nil @c1.i2

    # pass good parameters, using Time
    v2=Time.now
    assert_valid  do @c1.miaw(4, v2) end
    assert_equal 4, @c1.i1
    assert_equal v2, @c1.i2

    # pass good parameters, using DateTime
    v2=DateTime.now
    assert_valid  do @c1.miaw(4, v2) end
    assert_equal 4, @c1.i1
    assert_equal v2, @c1.i2

    
  end

  def test_q_multivar_with_validate_arg
    assert_nil @c1.i1
    assert_nil @c1.i2
    
    # a valid value for second param
    v2=Time.now
    
    # pass a wrong first param
    assert_invalid(:ArgumentType)  do @c1.qiaw(true, v2) end
    assert_nil @c1.i1
    assert_nil @c1.i2

    # pass a wrong second param
    assert_invalid(:ArgumentType)  do @c1.qiaw(4, 4) end
    assert_nil @c1.i1
    assert_nil @c1.i2

    # pass good parameters, using Time
    v2=Time.now
    assert_valid  do @c1.qiaw(4, v2) end
    assert_equal 4, @c1.i1
    assert_equal v2, @c1.i2

    # pass good parameters, using DateTime
    v2=DateTime.now
    assert_valid  do @c1.qiaw(4, v2) end
    assert_equal 4, @c1.i1
    assert_equal v2, @c1.i2

    
  end

  def test_kw_multivar_with_validate_arg
    assert_nil @c1.i1
    assert_nil @c1.i2
    
    # a valid value for second param
    v2x=Time.now
    
    # pass a wrong v1 param
    assert_invalid(:ArgumentType)  do @c1.kiaw(v1: true, v2: v2x,) end
    assert_nil @c1.i1
    assert_nil @c1.i2
    # order should not matter 
    assert_invalid(:ArgumentType)  do @c1.kiaw(v2: 4, v1: v2x ) end
    assert_nil @c1.i1
    assert_nil @c1.i2
    # pass a wrong v2 param
    assert_invalid(:ArgumentType)  do @c1.kiaw(v2: 4, v1: 4) end
    assert_nil @c1.i1
    assert_nil @c1.i2

    # pass good parameters, using Time
    v2x=Time.now
    assert_valid  do @c1.kiaw(v1: 4, v2: v2x) end
    assert_equal 4, @c1.i1
    assert_equal v2x, @c1.i2

    # pass good parameters, using DateTime
    v2x=DateTime.now
    assert_valid  do @c1.kiaw(v2: v2x, v1: 4) end
    assert_equal 4, @c1.i1
    assert_equal v2x, @c1.i2

    
  end

  
  # we cant validate default values yet,  because they 
  # are not accessible easily outside of the passed block...
  def test_defv_multivar_default_with_validate_arg
    assert_nil @c1.i1
    assert_nil @c1.i2
        
#    # pass a wrong first param
#    assert_invalid(:ArgumentType)  do @c1.m1aw(6) end
#    assert_nil @c1.i1
#    assert_nil @c1.i2

    assert_valid do @c1.m1aw(6) end
    assert_equal 6, @c1.i1
    assert_equal 'x2', @c1.i2

    # a valid value for second param
    v2=Time.now
    assert_valid  do @c1.m1aw(7, v2) end
    assert_equal 7, @c1.i1
    assert_equal v2, @c1.i2

  end

  # we cant validate default values yet,  because they 
  # are not accessible easily outside of the passed block...
  def test_q_multivar_default_with_validate_arg
    assert_nil @c1.i1
    assert_nil @c1.i2
        
#    # pass a wrong first param
#    assert_invalid(:ArgumentType)  do @c1.m1aw(6) end
#    assert_nil @c1.i1
#    assert_nil @c1.i2

    assert_valid do @c1.q1aw(6) end
    assert_equal 6, @c1.i1
    assert_equal 'x2', @c1.i2

    # a valid value for second param
    v2=Time.now
    assert_valid  do @c1.q1aw(7, v2) end
    assert_equal 7, @c1.i1
    assert_equal v2, @c1.i2

  end



  # we cant validate default values yet,  because they 
  # are not accessible easily outside of the passed block...
  def test_kw_multivar_default_with_validate_arg
    assert_nil @c1.i1
    assert_nil @c1.i2
        
#    # pass a wrong first param
#    assert_invalid(:ArgumentType)  do @c1.m1aw(v1: 6) end
#    assert_nil @c1.i1
#    assert_nil @c1.i2

    assert_valid do @c1.k1aw(v1: 6) end
    assert_equal 6, @c1.i1
    assert_equal 'x2', @c1.i2

    # a valid value for second param
    v2x=Time.now
    assert_valid  do @c1.k1aw(v1: 7, v2: v2x) end
    assert_equal 7, @c1.i1
    assert_equal v2x, @c1.i2

  end


end

class TC_Singleton_Method < Test::Unit::TestCase

  def teardown
    @c1 = nil
    @another = nil
  end
  
  def test_singleton
    # hack because test/unit runs this here,
    # but we want to run it only in subclasser where @c1 is setup
    return unless @c1
    
    [:mia,:kia,:miav, :qiav, :kiav,:miaw, :qiaw, :kiaw,
               :m1aw, :q1aw, :k1aw,:r1, :rq1, :rwrong, :rqwrong].each{|methn|
               
      assert  @c1.respond_to?(methn),"#{methn} not defined for #{@c1}"
      
      assert_false @another.respond_to?(methn),"#{methn} defined for #{@another} but it should not (singleton meth)"
    }
  end
  
end
class TC_Singleton_Validate_Result_Raise  < TC_Singleton_Method

  def setup
    @c1 = C6_4_SingletonTest.new
    @c1.CR6_Singleton()
    
    @another = C6_4_SingletonTest.new
  end

  include TM_Validate_Asserts_With_Exceptions
  include TM_Singleton_Validate_Result 
end

class TC_Singleton_Validate_Result_Raise_L  < TC_Singleton_Method
  def setup
    @c1 = C6L_4_SingletonTest.new
    @c1.LR6_Singleton()
    @another = C6L_4_SingletonTest.new
  end
  
  include TM_Validate_Asserts_With_Exceptions
  include TM_Singleton_Validate_Result 
end


class TC_Singleton_Validate_Result_Retwra  < TC_Singleton_Method
  def setup
    @c1 = E6_4_SingletonTest.new
    @c1.CR6_Singleton()
    @another = E6_4_SingletonTest.new
  end
  include TM_Validate_Asserts_With_Retwra
  include TM_Singleton_Validate_Result
end

class TC_Singleton_Validate_Result_Retwra_L < TC_Singleton_Method
  def setup
    @c1 = E6L_4_SingletonTest.new
    @c1.LR6_Singleton()
    @another = E6L_4_SingletonTest.new
  end
  include TM_Validate_Asserts_With_Retwra
  include TM_Singleton_Validate_Result
end

class TC_Singleton_Validate_Result_Retor  < TC_Singleton_Method
  def setup
    @c1 = O6_4_SingletonTest.new
    @c1.CR6_Singleton()
    @another = O6_4_SingletonTest.new
  end
  include TM_Validate_Asserts_With_Retor
  include TM_Singleton_Validate_Result
end

class TC_Singleton_Validate_Result_Retor_L < TC_Singleton_Method
  def setup
    @c1 = O6L_4_SingletonTest.new
    @c1.LR6_Singleton()
    @another = O6L_4_SingletonTest.new
  end
  include TM_Validate_Asserts_With_Retor
  include TM_Singleton_Validate_Result
end

class TC_Singleton_Validate_Args_Raise  < TC_Singleton_Method
  def setup
    @c1 = C6_4_SingletonTest.new
    @c1.CR6_Singleton()
    @another = C6_4_SingletonTest.new
  end
  include TM_Validate_Asserts_With_Exceptions
  include TM_Singleton_Validate_Args_Noerr
  include TM_Singleton_Validate_Args
  
end
class TC_Singleton_Validate_Args_Raise_L  < TC_Singleton_Method
  def setup
    @c1 = C6L_4_SingletonTest.new
    @c1.LR6_Singleton()
    @another = C6L_4_SingletonTest.new
  end
  include TM_Validate_Asserts_With_Exceptions
  include TM_Singleton_Validate_Args_Noerr  
  include TM_Singleton_Validate_Args
  
end

class TC_Singleton_Validate_Args_Retwra  < TC_Singleton_Method
  def setup
    @c1 = E6_4_SingletonTest.new
    @c1.CR6_Singleton()
    @another = E6_4_SingletonTest.new
  end
  include TM_Validate_Asserts_With_Retwra
  include TM_Singleton_Validate_Args
  
end

class TC_Singleton_Validate_Args_Retwra_L  < TC_Singleton_Method
  def setup
    @c1 = E6L_4_SingletonTest.new
    @c1.LR6_Singleton()
    @another = E6L_4_SingletonTest.new
  end
  
  include TM_Validate_Asserts_With_Retwra
  include TM_Singleton_Validate_Args
  
end

class TC_Singleton_Validate_Args_Retor  < TC_Singleton_Method
  def setup
    @c1 = O6_4_SingletonTest.new
    @c1.CR6_Singleton()
    @another = O6_4_SingletonTest.new
  end
  include TM_Validate_Asserts_With_Retor
  include TM_Singleton_Validate_Args
  
end

class TC_Singleton_Validate_Args_Retor_L  < TC_Singleton_Method
  def setup
    @c1 = O6L_4_SingletonTest.new
    @c1.LR6_Singleton()
    @another = O6L_4_SingletonTest.new
  end
  
  include TM_Validate_Asserts_With_Retor
  include TM_Singleton_Validate_Args
  
end