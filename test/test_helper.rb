require_relative '../lib/tchae'
require 'test/unit'
require 'pp'

module X6
  @cvar = nil
 
  attr_reader :ivar
  attr_reader :i1
  attr_reader :i2
end

module W6_EX
  def tea_wrapper
    Tchae.Wrapper(::Tchae::Handling::RAISE)
  end
end

module W6_RETWRA
  def tea_wrapper
    Tchae.Wrapper(::Tchae::Handling::RETURN_WRAPPER)
  end
end

module W6_RETOR
  def tea_wrapper
    Tchae.Wrapper(::Tchae::Handling::RETURN_RESULT_OR_ERROR)
  end
end

module Y6 
  def w1
    tea_wrapper.args.positional.expect do arg{ is_a? Integer} end
  end
  
  #should be same as w1 but written differently
  def q1
    tea_wrapper.args.positional.expect( ->{ is_a? Integer} ) 
  end
  
  def k1 
    tea_wrapper.args.keyword.expect nval: ->{ is_a? Integer} 
  end
  
  def w2 
    tea_wrapper.args.positional.expect do 
      arg{ is_a? Integer}
      arg{ class_respond_to? :now} 
    end
  end
  
  #same as w2
  def q2 
    tea_wrapper.args.positional.expect(->{ is_a? Integer},
                                       ->{ class_respond_to? :now}) 
    
  end
  
  def k2
    tea_wrapper.args.keyword.expect v1: ->{ is_a? Integer},
                                       v2: ->{ class_respond_to? :now} 
  end
  
  def w3
    tea_wrapper.args.positional.expect do 
      arg{ is_a? Integer}
    end.result.expect{ is_a? String }
  end
  
  def q3
    tea_wrapper.args.positional.expect(->{ is_a? Integer}).result.expect{ is_a? String }
  end
  
  def w4 
    tea_wrapper.args.positional.expect do 
      arg{ is_a? Integer}
      arg{ is_a? String}
    end.result.expect{ is_a? String }
  end

  def q4
    tea_wrapper.args.positional.expect(->{ is_a? Integer}, ->{ is_a? String}).result.expect { is_a? String }
  end
  
end

module TM_Validate_Asserts_With_Exceptions
  def assert_invalid(invalid_type, &proc)
    expected_exception = Tchae.const_get "#{invalid_type}Exception"
    assert_raise(expected_exception, &proc)
  end
  def assert_valid(&proc)
    proc.call
    assert_nothing_raised(&proc)
  end
  def assert_valid_equal(expected, &proc)
    assert_nothing_raised do
      x = proc.call
      assert_equal expected, x
    end
  end
end

# Assert for when a Result-wrapper is used 
module TM_Validate_Asserts_With_Retwra
  def assert_invalid(invalid_type, &proc)
    expected_error = Tchae.const_get "#{invalid_type}Error"
    assert_nothing_raised do
      x = proc.call
    
      assert x.respond_to? :result
      assert x.respond_to? :error
      assert_nil x.result
      assert_kind_of expected_error, x.error
    end
  end
  def assert_valid(&proc)
      x = proc.call
    assert_nothing_raised do
      x = proc.call
      assert x.respond_to? :result, "Returned wrapper is a #{x.class}"
      assert x.respond_to? :error
      assert_nil x.error
    end
  end
  def assert_valid_equal(expected_return, &proc)
    assert_nothing_raised do
      x = proc.call
    
      assert x.respond_to? :result
      assert x.respond_to? :error
      assert_nil x.error
      assert_equal expected_return, x.result
    end
  end
end 

# Assert for when no Result-wrapper is used , but either Valid-result or 
# Error is returned
module TM_Validate_Asserts_With_Retor
  def assert_invalid(invalid_type, &proc)
    expected_error = Tchae.const_get "#{invalid_type}Error"
    assert_nothing_raised do
      x = proc.call
      assert_kind_of expected_error, x
    end
  end
  def assert_valid(&proc)
    assert_nothing_raised do
      x = proc.call
      assert_false x.kind_of? ::Tchae::Error
    end
  end
  def assert_valid_equal(expected_return, &proc)
    assert_nothing_raised do
      x = proc.call
      assert_false x.kind_of? ::Tchae::Error
      assert_equal expected_return, x
    end
  end
end 
