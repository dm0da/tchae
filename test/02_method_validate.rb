require_relative './test_helper'
require 'date'


def CR6
# instance method mia that receives an argument without validation
  create_validated_method(:mia){ |nval| @ivar = nval }
  create_validated_method(:kia){ | nval: | @ivar = nval }
  
# instance method miav that receives an argument with Validation 
# is_a?(Integer)
  create_validated_method(:miav, w1){ |nval| @ivar = nval}
  create_validated_method(:qiav, q1){ |nval| @ivar = nval}
  create_validated_method(:kiav, k1){ |nval: | @ivar = nval}
  
# instance method miaw that receives two arguments with Validation 
#   is_a?(Integer) and class_respond_to?(:now)
  create_validated_method(:miaw, w2){ |v1,v2| @i1 = v1 ;   @i2 = v2  }
  create_validated_method(:qiaw, q2){ |v1,v2| @i1 = v1 ;   @i2 = v2  }
  create_validated_method(:kiaw, k2){ |v1: , v2: | @i1 = v1 ;   @i2 = v2  }
  
# same as miaw with a wrong default value for second optional param 
  create_validated_method(:m1aw, w2) { |v1, v2='x2'| @i1 = v1; @i2 = v2  }
  create_validated_method(:q1aw, q2) { |v1, v2='x2'| @i1 = v1; @i2 = v2  }
  create_validated_method(:k1aw, k2) { |v1: , v2: 'x2'| @i1 = v1; @i2 = v2  }
  
# Validating method result
# in: Integer, out: String
  create_validated_method(:r1, w3){ |i|  i.to_s  }
  create_validated_method(:rq1, q3){ |i|  i.to_s  }
  # simulate a programing error

  create_validated_method(:rwrong, w4){|i,s| if false then s else i end }
  create_validated_method(:rqwrong, q4){|i,s| if false then s else i end }

end

def LR6
# instance method mia that receives an argument without validation
  create_validated_method :mia, lambda: ->(nval){ @ivar = nval }
  create_validated_method :kia, lambda: ->(nval: ){ @ivar = nval }
   
# instance method miav that receives an argument with Validation 
# is_a?(Integer)
  create_validated_method :miav, w1, lambda: ->(nval){  @ivar = nval}
  create_validated_method :qiav, q1, lambda: ->(nval){  @ivar = nval}
  create_validated_method :kiav, k1, lambda: ->(nval: ){  @ivar = nval}

# instance method miaw that receives two arguments with Validation 
#   is_a?(Integer) and class_respond_to?(:now)
  create_validated_method :miaw, w2, lambda: ->(v1,v2){ 
    @i1 = v1 ;   @i2 = v2   
  }
  create_validated_method :qiaw, q2, lambda: ->(v1,v2){ 
    @i1 = v1 ;   @i2 = v2   
  }
  create_validated_method :kiaw, k2, lambda: ->(v1:, v2:){ 
    @i1 = v1 ;   @i2 = v2   
  }
# same as miaw with a wrong default value for second optional param 
  create_validated_method :m1aw, w2, lambda: ->(v1,v2='x2'){ 
    @i1 = v1; @i2 = v2  
  } 
  create_validated_method :q1aw, q2, lambda: ->(v1,v2='x2'){ 
    @i1 = v1; @i2 = v2  
  } 
  create_validated_method :k1aw, k2, lambda: ->(v1: , v2: 'x2'){ 
    @i1 = v1; @i2 = v2  
  } 

# Validating method result
# in: Integer, out: String
  create_validated_method :r1, w3, lambda: ->(i){  i.to_s  }
  create_validated_method :rq1, q3, lambda: ->(i){  i.to_s  }
  
  # simulate a programing error  
  create_validated_method :rwrong, w4, lambda: ->(i,s){
   if false then s else i end 
  }
  create_validated_method :rqwrong, q4, lambda: ->(i,s){
   if false then s else i end 
  }

end


class C6
  
  include X6
  extend W6_EX
  extend Y6
  CR6()
  
end

class C6L

  include X6
  extend W6_EX
  extend Y6
  LR6()
  
end

class E6
  
  include X6
  extend W6_RETWRA
  extend Y6
  CR6()
  
end

class E6L

  include X6
  extend W6_RETWRA
  extend Y6
  LR6()
  
end

class O6
  
  include X6
  extend W6_RETOR
  extend Y6
  CR6()
  
end

class O6L

  include X6
  extend W6_RETOR
  extend Y6
  LR6()
  
end


module TM_Validate_Result
  def test_result_ok
    assert_valid_equal('1') do @c1.r1(1)  end
    assert_invalid(:ArgumentType) do   @c1.r1('1')   end
    
    assert_valid_equal('1') do @c1.rq1(1)  end
    assert_invalid(:ArgumentType) do   @c1.rq1('1')   end
  end
  
  
  def test_result_notok
    assert_invalid(:ReturnType) do  @c1.rwrong(1,'2')   end
    assert_invalid(:ReturnType) do  @c1.rqwrong(1,'2')   end
  end

end

module TM_Validate_Args_Noerr
# note: these tests are with methods created with a Nil method wrapper
#       thus having the RAISE invalid-handling per default,
#       thus they must be tested with the RAISE asserts only
  def test_defv_ivar_wo_validate_arg
    assert_nil @c1.ivar
    assert_valid_equal(true) do @c1.mia(true) end
    assert @c1.ivar

    assert_valid_equal(false) do @c1.mia(false) end
    assert_false @c1.ivar

  end
  
  def test_kw_ivar_wo_validate_arg
    assert_nil @c1.ivar
    assert_valid_equal(true) do @c1.kia(nval: true) end
    assert @c1.ivar

    assert_false @c1.kia(nval: false)
    assert_false @c1.ivar

    
  end
end

module TM_Validate_Args

  def test_defv_ivar_with_validate_arg
    assert_nil @c1.ivar
    assert_invalid(:ArgumentType) do @c1.miav(true) end
    assert_nil @c1.ivar

    assert_valid_equal(2) do @c1.miav(2) end
    assert_equal 2, @c1.ivar

    
  end
  def test_q_ivar_with_validate_arg
    assert_nil @c1.ivar
    assert_invalid(:ArgumentType) do @c1.qiav(true) end
    assert_nil @c1.ivar

    assert_valid_equal(2) do @c1.qiav(2) end
    assert_equal 2, @c1.ivar

    
  end
  def test_kw_ivar_with_validate_arg
    assert_nil @c1.ivar
    assert_invalid(:ArgumentType) do @c1.kiav(nval: true) end
    assert_nil @c1.ivar

    assert_valid_equal(2) do @c1.kiav(nval: 2) end
    assert_equal 2, @c1.ivar

    
  end


  def test_defv_multivar_with_validate_arg
    assert_nil @c1.i1
    assert_nil @c1.i2
    
    # a valid value for second param
    v2=Time.now
    
    # pass a wrong first param
    assert_invalid(:ArgumentType)  do @c1.miaw(true, v2) end
    assert_nil @c1.i1
    assert_nil @c1.i2

    # pass a wrong second param
    assert_invalid(:ArgumentType)  do @c1.miaw(4, 4) end
    assert_nil @c1.i1
    assert_nil @c1.i2

    # pass good parameters, using Time
    v2=Time.now
    assert_valid  do @c1.miaw(4, v2) end
    assert_equal 4, @c1.i1
    assert_equal v2, @c1.i2

    # pass good parameters, using DateTime
    v2=DateTime.now
    assert_valid  do @c1.miaw(4, v2) end
    assert_equal 4, @c1.i1
    assert_equal v2, @c1.i2

    
  end

  def test_q_multivar_with_validate_arg
    assert_nil @c1.i1
    assert_nil @c1.i2
    
    # a valid value for second param
    v2=Time.now
    
    # pass a wrong first param
    assert_invalid(:ArgumentType)  do @c1.qiaw(true, v2) end
    assert_nil @c1.i1
    assert_nil @c1.i2

    # pass a wrong second param
    assert_invalid(:ArgumentType)  do @c1.qiaw(4, 4) end
    assert_nil @c1.i1
    assert_nil @c1.i2

    # pass good parameters, using Time
    v2=Time.now
    assert_valid  do @c1.qiaw(4, v2) end
    assert_equal 4, @c1.i1
    assert_equal v2, @c1.i2

    # pass good parameters, using DateTime
    v2=DateTime.now
    assert_valid  do @c1.qiaw(4, v2) end
    assert_equal 4, @c1.i1
    assert_equal v2, @c1.i2

    
  end


  def test_kw_multivar_with_validate_arg
    assert_nil @c1.i1
    assert_nil @c1.i2
    
    # a valid value for second param
    v2x=Time.now
    
    # pass a wrong v1 param
    assert_invalid(:ArgumentType)  do @c1.kiaw(v1: true, v2: v2x,) end
    assert_nil @c1.i1
    assert_nil @c1.i2
    # order should not matter 
    assert_invalid(:ArgumentType)  do @c1.kiaw(v2: 4, v1: v2x ) end
    assert_nil @c1.i1
    assert_nil @c1.i2
    # pass a wrong v2 param
    assert_invalid(:ArgumentType)  do @c1.kiaw(v2: 4, v1: 4) end
    assert_nil @c1.i1
    assert_nil @c1.i2

    # pass good parameters, using Time
    v2x=Time.now
    assert_valid  do @c1.kiaw(v1: 4, v2: v2x) end
    assert_equal 4, @c1.i1
    assert_equal v2x, @c1.i2

    # pass good parameters, using DateTime
    v2x=DateTime.now
    assert_valid  do @c1.kiaw(v2: v2x, v1: 4) end
    assert_equal 4, @c1.i1
    assert_equal v2x, @c1.i2

    
  end

  
  # we cant validate default values yet,  because they 
  # are not accessible easily outside of the passed block...
  def test_defv_multivar_default_with_validate_arg
    assert_nil @c1.i1
    assert_nil @c1.i2
        
#    # pass a wrong first param
#    assert_invalid(:ArgumentType)  do @c1.m1aw(6) end
#    assert_nil @c1.i1
#    assert_nil @c1.i2

    assert_valid do @c1.m1aw(6) end
    assert_equal 6, @c1.i1
    assert_equal 'x2', @c1.i2

    # a valid value for second param
    v2=Time.now
    assert_valid  do @c1.m1aw(7, v2) end
    assert_equal 7, @c1.i1
    assert_equal v2, @c1.i2

  end
  # we cant validate default values yet,  because they 
  # are not accessible easily outside of the passed block...
  def test_q_multivar_default_with_validate_arg
    assert_nil @c1.i1
    assert_nil @c1.i2
        
#    # pass a wrong first param
#    assert_invalid(:ArgumentType)  do @c1.m1aw(6) end
#    assert_nil @c1.i1
#    assert_nil @c1.i2

    assert_valid do @c1.q1aw(6) end
    assert_equal 6, @c1.i1
    assert_equal 'x2', @c1.i2

    # a valid value for second param
    v2=Time.now
    assert_valid  do @c1.q1aw(7, v2) end
    assert_equal 7, @c1.i1
    assert_equal v2, @c1.i2

  end

  # we cant validate default values yet,  because they 
  # are not accessible easily outside of the passed block...
  def test_kw_multivar_default_with_validate_arg
    assert_nil @c1.i1
    assert_nil @c1.i2
        
#    # pass a wrong first param
#    assert_invalid(:ArgumentType)  do @c1.m1aw(v1: 6) end
#    assert_nil @c1.i1
#    assert_nil @c1.i2

    assert_valid do @c1.k1aw(v1: 6) end
    assert_equal 6, @c1.i1
    assert_equal 'x2', @c1.i2

    # a valid value for second param
    v2x=Time.now
    assert_valid  do @c1.k1aw(v1: 7, v2: v2x) end
    assert_equal 7, @c1.i1
    assert_equal v2x, @c1.i2

  end


end

class TC_NotSingleton_Method < Test::Unit::TestCase

  def teardown
    @c1 = nil
    @another = nil
  end
  
  def test_not_singleton
    # hack because test/unit runs this here,
    # but we want to run it only in subclasses where @c1 is setup
    return unless @c1
    
    [:mia,:kia,:miav, :qiav, :kiav,:miaw, :qiaw, :kiaw,
               :m1aw, :q1aw, :k1aw,:r1, :rq1, :rwrong, :rqwrong].each{|methn|
               
      assert  @c1.respond_to?(methn),"#{methn} not defined for #{@c1}"
      
      assert @another.respond_to?(methn),"#{methn} defined for #{@another} but it should (not singleton meth)"
    }
  end
  
end

class TC_Validate_Result_Raise  < TC_NotSingleton_Method
  def setup
    @c1 = C6.new
    @another = C6.new
  end
  include TM_Validate_Asserts_With_Exceptions
  include TM_Validate_Result 
end

class TC_Validate_Result_Raise_L  < TC_NotSingleton_Method
  def setup
    @c1 = C6L.new
    @another = C6L.new
  end
  include TM_Validate_Asserts_With_Exceptions
  include TM_Validate_Result 
end

class TC_Validate_Result_Retwra  < TC_NotSingleton_Method
  def setup
    @c1 = E6.new
    @another = E6.new
  end
  include TM_Validate_Asserts_With_Retwra
  include TM_Validate_Result
end

class TC_Validate_Result_Retwra_L  < TC_NotSingleton_Method
  def setup
    @c1 = E6L.new
    @another = E6L.new
  end
  include TM_Validate_Asserts_With_Retwra
  include TM_Validate_Result
end

class TC_Validate_Result_Retor  < TC_NotSingleton_Method
  def setup
    @c1 = O6.new
    @another = O6.new
  end
  include TM_Validate_Asserts_With_Retor
  include TM_Validate_Result
end

class TC_Validate_Result_Retor_L  < TC_NotSingleton_Method
  def setup
    @c1 = O6L.new
    @another = O6L.new
  end
  include TM_Validate_Asserts_With_Retor
  include TM_Validate_Result
end


class TC_Validate_Args_Raise  < TC_NotSingleton_Method
  def setup
    @c1 = C6.new
    @another = C6.new
  end
  include TM_Validate_Asserts_With_Exceptions
  include TM_Validate_Args_Noerr
  include TM_Validate_Args
  
end

class TC_Validate_Args_Raise_L  < TC_NotSingleton_Method
  def setup
    @c1 = C6L.new
    @another = C6L.new
  end
  include TM_Validate_Asserts_With_Exceptions
  include TM_Validate_Args_Noerr  
  include TM_Validate_Args
  
end

class TC_Validate_Args_Retwra  < TC_NotSingleton_Method
  def setup
    @c1 = E6.new
    @another = E6.new
  end
  include TM_Validate_Asserts_With_Retwra
  include TM_Validate_Args
  
end
class TC_Validate_Args_Retwra_L  < TC_NotSingleton_Method
  def setup
    @c1 = E6L.new
    @another = E6L.new
  end
  include TM_Validate_Asserts_With_Retwra
  include TM_Validate_Args
  
end

class TC_Validate_Args_Retor < TC_NotSingleton_Method
  def setup
    @c1 = O6.new
    @another = O6.new
  end
  include TM_Validate_Asserts_With_Retor
  include TM_Validate_Args
  
end
class TC_Validate_Args_Retor_L  < TC_NotSingleton_Method
  def setup
    @c1 = O6L.new
    @another = O6L.new
  end
  include TM_Validate_Asserts_With_Retor
  include TM_Validate_Args
  
end


