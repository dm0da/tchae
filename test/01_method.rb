require_relative './test_helper'

class C1L
  @cvar = nil
  class << self
    # class method mc that does nothing
    create_validated_method :mc,lambda: ->{}
    # class method mr that just returns a symbol
    create_validated_method :mr, lambda: ->{  :mrc_return  }
    
    def getcvar
      @cvar
    end    

    # class method mcvartrue that sets value of class @cvar to true
    create_validated_method :mcvartrue,lambda: ->{ @cvar = true }


  end

  attr_reader :ivar

# instance method m1 that does nothing
  create_validated_method :m1, lambda: ->{}


# instance method mr that just returns a symbol (without return keyword)
  create_validated_method :mr, lambda: ->{ :mr_return }

# instance method mr18 that explicitely returns a symbol 
  create_validated_method :mr18,lambda: ->{ return '18+' }
  
  
  # instance method mivartrue that sets value of @ivar to true
  create_validated_method :mivartrue, lambda: ->{  @ivar = true }
end



class C2L
  @cvar = nil
  class << self
    # class method m2 that does nothing
    create_validated_method :m2, lambda: ->{}
    # class method mr that just returns a symbol
    create_validated_method :mr,lambda: ->{  :mrc2_return  }
    
    def getcvar
      @cvar
    end
  end
end

class C4L
  @cvar = nil

  attr_reader :ivar

# instance method mia that receives an argument
  create_validated_method :mia, lambda: ->(nval){ @ivar = nval}
end

class C1
  @cvar = nil
  class << self
    # class method mc that does nothing
    create_validated_method :mc
    # class method mr that just returns a symbol
    create_validated_method :mr do  :mrc_return  end
    
    def getcvar
      @cvar
    end    

    # class method mcvartrue that sets value of class @cvar to true
    create_validated_method :mcvartrue do @cvar = true end


  end

  attr_reader :ivar

# instance method m1 that does nothing
  create_validated_method :m1


# instance method mr that just returns a symbol (without return keyword)
  create_validated_method :mr do :mr_return end

# instance method mr18 that explicitely returns a symbol 
  create_validated_method :mr18 do return '18+' end
  
  
  # instance method mivartrue that sets value of @ivar to true
  create_validated_method :mivartrue do  @ivar = true end
end



class C2
  @cvar = nil
  class << self
    # class method m2 that does nothing
    create_validated_method :m2
    # class method mr that just returns a symbol
    create_validated_method :mr do :mrc2_return  end
    
    def getcvar
      @cvar
    end
  end
end

class C4
  @cvar = nil

  attr_reader :ivar

# instance method mia that receives an argument
  create_validated_method :mia do |nval| @ivar = nval end
end


# re-useable tests
module  TM_Basic_Method_0
  
  def test_create_class_method
    assert @tk1.methods.include?(:mc)
    assert_nil @tk1.mc
    assert @tk2.methods.include?(:m2)
    assert_nil @tk2.m2
  end
  def test_create_instance_method
    assert @tk1.instance_methods.include? :m1
    assert_nil @c1.m1
  end
  
 
end



# re-useable tests
module  TM_Basic_Method_1
  

  def test_return_implicit
    assert_equal :mr_return, @c1.mr
    assert_equal :mrc_return, @tk1.mr
    assert_equal :mrc2_return, @tk2.mr
  end
  
  def test_return_explicit
    assert_equal '18+', @c1.mr18
  end
  
  def test_instance_var_access
    assert_nil @c1.ivar
    assert @c1.mivartrue
    assert @c1.ivar
  end

  def test_class_var_access
    assert_nil @tk1.getcvar
    assert @tk1.mcvartrue
    assert @tk1.getcvar
  end

end

class TC_Basic_Method_0  < Test::Unit::TestCase
  
  def setup
    @tk1 = C1  #test-class for instance methods
    @tk2 = C2 #test-class for class-methods
    @c1 = @tk1.new
  end
  def teardown
    @c1 = nil
  end
  include TM_Basic_Method_0
end
class TC_Basic_Method_0_L  < Test::Unit::TestCase
  
  def setup
    @tk1 = C1L  #test-class for instance methods
    @tk2 = C2L #test-class for class-methods
    @c1 = @tk1.new
  end
  def teardown
    @c1 = nil
  end
  include TM_Basic_Method_0
end

class TC_Basic_Method_1  < Test::Unit::TestCase
  
  def setup
    @tk1 = C1
    @tk2 = C2
    @c1 = @tk1.new
  end
  def teardown
    @c1 = nil
  end
  include TM_Basic_Method_1
end

class TC_Basic_Method_1_L  < Test::Unit::TestCase
  
  def setup
    @tk1 = C1L
    @tk2 = C2L
    @c1 = @tk1.new
  end
  def teardown
    @c1 = nil
  end
  include TM_Basic_Method_1
end

module TM_Basic_Method_Args

  def test_method_instance_var_mutation
    assert_nil @c1.ivar
    assert @c1.mia(true)
    assert @c1.ivar

    assert_false @c1.mia(false)
    assert_false @c1.ivar

    
  end

end

class TC_Basic_Method_Args  < Test::Unit::TestCase
  def setup
    @tk1 = C4
    @c1 = @tk1.new
  end
  def teardown
    @c1 = nil
  end

  include TM_Basic_Method_Args
end

class TC_Basic_Method_Args_L  < Test::Unit::TestCase
  def setup
    @tk1 = C4L
    @c1 = @tk1.new
  end
  def teardown
    @c1 = nil
  end

  include TM_Basic_Method_Args
end