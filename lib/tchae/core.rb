class Object
  def class_respond_to?(*args)
    self.class.respond_to?(*args)
  end
end

module Tchae

  def self.Wrapper(handling = ::Tchae::Handling::RAISE)
    MethodValidator.new(handling)
  end

  #  wrap method with validation, and on invalid,
  #  exit with control flow and retun a wrapped error object
  # on success return a wrapped return object
  def self.do_validation_return_wrapper(obj, wrapper, tea_methodsymb,
                                       proclmbda, params, kwparms)
    unless params.empty?
      if (err = wrapper.args.positional.valid_or_return_error(params))
        return Return(nil, err)
      end
    end
    unless kwparms.empty?
      if (err = wrapper.args.keyword.valid_or_return_error(kwparms))
        return Return(nil, err)
      end
    end

    unchecked_result = if proclmbda.arity.zero?
                         obj.__send__(tea_methodsymb)
                       elsif kwparms.empty?
                         obj.__send__(tea_methodsymb, *params)
                       elsif params.empty?
                         obj.__send__(tea_methodsymb, **kwparms)
                       else
                         obj.__send__(tea_methodsymb, *params, **kwparms)
                       end

    wrapper.result.return_wrapper unchecked_result
  end
  
  #  wrap method with validation, and on invalid,
  #  exit with control flow and retun a error object
  # on success return the valid result
  def self.do_validation_result_or_error(obj, wrapper, tea_methodsymb,
                                    proclmbda, params, kwparms)
    unless params.empty?
      if (err = wrapper.args.positional.valid_or_return_error(params))
        return err
      end
    end
    unless kwparms.empty?
      if (err = wrapper.args.keyword.valid_or_return_error(kwparms))
        return err
      end
    end

    unchecked_result = if proclmbda.arity.zero?
                         obj.__send__(tea_methodsymb)
                       elsif kwparms.empty?
                         obj.__send__(tea_methodsymb, *params)
                       elsif params.empty?
                         obj.__send__(tea_methodsymb, **kwparms)
                       else
                         obj.__send__(tea_methodsymb, *params, **kwparms)
                       end

    wrapper.result.valid_or_return_error unchecked_result
  end

  #  wrap method with validation, and on invalid,
  #  exit by raising exception

  def self.do_validation_raise(obj, wrapper, tea_methodsymb,
                                    proclmbda, params, kwparms)

    wrapper.args.positional.valid_or_raise_exception(params)
    wrapper.args.keyword.valid_or_raise_exception(kwparms)
    unchecked_result = if proclmbda.arity.zero?
                         obj.__send__(tea_methodsymb)
                       elsif kwparms.empty?
                         obj.__send__(tea_methodsymb, *params)
                       elsif params.empty?
                         obj.__send__(tea_methodsymb, **kwparms)
                       else
                         obj.__send__(tea_methodsymb, *params, **kwparms)
                       end
    wrapper.result.valid_or_raise_exception unchecked_result
  end

  class Error
    attr_reader :msg
    def initialize(input); end
  end

  class ReturnTypeError < Error
  end

  class ArgumentValueError < Error
  end

  class ArgumentTypeError < Error
  end

  class ReturnTypeException < TypeError
  end

  class ArgumentValueException < ArgumentError
  end

  class ArgumentTypeException < TypeError
  end

  class Validator
    attr_reader :message

    def initialize(proc, msg: nil)
      @proc = proc
      @message = msg.nil? ? "does not fulfill #{proc}.to_s" : msg
    end

    def execute(input)
      input.instance_exec(&@proc)
    end

    def valid_or_raise_exception(input, eklass)
      raise eklass unless execute(input)

      input
    end

    def valid_or_return_error(input, eklass)
      execute(input) ? input : return_error(input, eklass)
    end

    def return_error(input, eklass)
      eklass.new(input)
    end

    def wrapped_result_or_error(result, eklass)
      execute(result) ? Return(result) : Return(nil, return_error(result, eklass))
    end
  end

  # same as Validator but with Arity 1 execution semantics
  class Validator1 < Validator
    def execute(input)
      @proc.call(input)
    end
  end

  class ResultWrapper
    attr_reader :result
    attr_reader :error
    def initialize(res, err = nil)
      @result = res
      @error = err
    end
  end

  class Result
    def initialize(wrapper)
      @wrapper = wrapper
      @valtor = nil
    end

    def expect(constraint = nil, &proc)
      @valtor = if block_given?
                  raise(ArgumentError, 'The Result.expect method accepts either a block or an argument but not both') if constraint

                  Tchae(proc)
                else
                  Tchae(constraint)
                end
      @wrapper
    end

    def valid_or_raise_exception(result)
      return result unless @valtor

      @valtor.valid_or_raise_exception(result, ::Tchae::ReturnTypeException)
    end

    def return_wrapper(result)
      return Return(result) unless @valtor

      @valtor.wrapped_result_or_error(result, ::Tchae::ReturnTypeError)
    end
    
    def valid_or_return_error(result)
      return result unless @valtor
      @valtor.valid_or_return_error(result, ::Tchae::ReturnTypeError)
    end
    
  end

  class PositionalArgs < Array
    def initialize(wrapper)
      super()
      @wrapper = wrapper
    end

    def arg(&proc)
      push Tchae(proc)
      @wrapper
    end

#    def expect(constraints = EMPTY_ARRAY, &proc)
    def expect(*constraints, &proc)
      constraints.each { |inp| push Tchae(inp) }
      instance_exec(&proc) if block_given?
      @wrapper
    end

    def valid_or_raise_exception(params)
      params.zip(self).each do |parm, valtor|
        next unless valtor

        valtor.valid_or_raise_exception(parm, ::Tchae::ArgumentTypeException)
      end
    end

    def valid_or_return_error(params)
      # returns nil if everything valid or an Error object otherwise
      valid = true
      failed = params.zip(self).each do |parm, valtor|
        next unless valtor

        unless valtor.execute(parm)
          valid = false
          break valtor.return_error(parm, ::Tchae::ArgumentTypeError)
        end
      end
      valid ? nil : failed
    end
  end

  class KeywordArgs < Hash
    def initialize(wrapper)
      super()
      @wrapper = wrapper
    end

    def expect(**keywprocs)
      keywprocs.each { |argsymb, proc| self[argsymb] = Tchae(proc) }
      @wrapper
    end

    def valid_or_raise_exception(keywps)
      each  do |key, valtor|
        next unless valtor
        next unless ((parm = keywps[key]) if keywps.key?(key))

        valtor.valid_or_raise_exception(parm, ::Tchae::ArgumentTypeException)
      end
    end

    def valid_or_return_error(keywps)
      # returns nil if everything valid or an Error object otherwise
      valid = true
      failed = each do |key, valtor|
        next unless valtor
        next unless ((parm = keywps[key]) if keywps.key?(key))

        unless valtor.execute(parm)
          valid = false
          break valtor.return_error(parm, ::Tchae::ArgumentTypeError)
        end
      end
      valid ? nil : failed
    end
  end

  class Arguments
    attr_reader :positional
    attr_reader :keyword
    def initialize(wrapper)
      @wrapper = wrapper
      @positional = PositionalArgs.new(wrapper)
      @keyword = KeywordArgs.new(wrapper)
    end
  end
  module Handling
    RAISE = :raise_exception
    RETURN_WRAPPER = :return_wrapper
    RETURN_RESULT_OR_ERROR = :return_result_or_error
    ALL = [RAISE, RETURN_WRAPPER, RETURN_RESULT_OR_ERROR].freeze
  end
end

# Central validator factory
def Tchae(inp)
  case inp
  when Class
    Tchae::Validator.new -> { is_a?(inp) }, msg: "is not a #{inp}"
  when Symbol
    Tchae::Validator.new -> { respond_to?(inp) }, msg: "does not respond to #{inp}"
  when Proc
    Tchae::Validator.new inp
  when Array
    Tchae::Validator1.new ->(value) { inp.find(value) }
  else
    Tchae::Validator.new inp.to_proc
  end
end

module Tchae
  class MethodValidator
    HANDLING_VTOR = Tchae(Handling::ALL).freeze
    attr_reader :result
    attr_reader :args
    attr_accessor :handling
    def initialize(handling = ::Tchae::Handling::RAISE)
      HANDLING_VTOR.valid_or_raise_exception(handling, ArgumentValueError)
      @result = Result.new(self)
      @args = Arguments.new(self)
      @handling = handling
    end
  end

  NilWrapper = MethodValidator.new.freeze

  module PI
    def create_validated_method(methodname,
                                wrapper = Tchae::NilWrapper,
                                lambda: nil,
                                &proc)
      proclmbda = if lambda.nil?
                    proc
                  else
                    raise ArgumentError, 'Please provide a block or the :lambda parameter but not both' if block_given?

                    lambda
                  end

      case wrapper.handling 
      when ::Tchae::Handling::RETURN_WRAPPER
        create_return_wrapper_method(methodname, wrapper, &proclmbda)
      when ::Tchae::Handling::RETURN_RESULT_OR_ERROR
        create_result_or_error_method(methodname, wrapper, &proclmbda)
      else # RAISE
        create_valid_or_raise_method(methodname, wrapper, &proclmbda)
      end
    end

    def create_valid_or_raise_method(methodname,
                                     wrapper = Tchae::NilWrapper, &proc)
      if block_given?
        tea_methodsymb = "__tea_#{methodname}".to_sym
        define_method "__tea_#{methodname}", proc
        define_method(methodname) do |*params, **kwparms|
          Tchae.do_validation_raise(self, wrapper,
                                       tea_methodsymb, proc,
                                       params, kwparms)
        end
      else
        define_method(methodname) {}
      end
    end

    def create_result_or_error_method(methodname, 
                                      wrapper= Tchae::NilWrapper, 
                                      &proc)
      if block_given?
        tea_methodsymb = "__tea_#{methodname}".to_sym
        define_method "__tea_#{methodname}", proc
        define_method(methodname) do |*params, **kwparms|
          Tchae.do_validation_result_or_error(self, wrapper,
                                              tea_methodsymb, proc,
                                              params, kwparms)
        end
      else
        define_method(methodname) {}
      end                                
    end
    
    def create_return_wrapper_method(methodname,
                                      wrapper = Tchae::NilWrapper,
                                      &proc)
      if block_given?
        tea_methodsymb = "__tea_#{methodname}".to_sym
        define_method "__tea_#{methodname}", proc
        define_method(methodname) do |*params, **kwparms|
          Tchae.do_validation_return_wrapper(self,
                                        wrapper,
                                        tea_methodsymb,
                                        proc,
                                        params,
                                        kwparms)
        end
      else
        define_method(methodname) {}
      end
    end
  end

  module PISingleton
    def create_validated_singleton_method(methodname,
                                          wrapper = Tchae::NilWrapper,
                                          lambda: nil,
                                          &proc)

      proclmbda = if lambda.nil?
                    proc
                  else
                    raise ArgumentError, 'Please provide a block or the :lambda parameter but not both' if block_given?

                    lambda
                  end

      case wrapper.handling 
      when ::Tchae::Handling::RETURN_WRAPPER
        create_return_wrapper_singleton_method(methodname, wrapper, &proclmbda)
      when ::Tchae::Handling::RETURN_RESULT_OR_ERROR
        create_result_or_error_singleton_method(methodname, wrapper, &proclmbda)
      else # RAISE
        create_valid_or_raise_singleton_method(methodname, wrapper, &proclmbda)
      end
    end

    def create_valid_or_raise_singleton_method(methodname,
                                               wrapper = Tchae::NilWrapper, &proc)
      if block_given?
        tea_methodsymb = "__tea_#{methodname}".to_sym
        define_singleton_method "__tea_#{methodname}", proc
        define_singleton_method(methodname) do |*params, **kwparms|
          Tchae.do_validation_raise(self, wrapper,
                                       tea_methodsymb, proc,
                                       params, kwparms)
        end
      else
        define_singleton_method(methodname) {}
      end
    end
    
    def create_result_or_error_singleton_method(methodname, 
                                                wrapper= Tchae::NilWrapper, 
                                                &proc)
      if block_given?
        tea_methodsymb = "__tea_#{methodname}".to_sym
        define_singleton_method "__tea_#{methodname}", proc
        define_singleton_method(methodname) do |*params, **kwparms|
          Tchae.do_validation_result_or_error(self, wrapper,
                                              tea_methodsymb, proc,
                                              params, kwparms)
        end
      else
        define_singleton_method(methodname) {}
      end                                          
    end
    
    def create_return_wrapper_singleton_method(methodname,
                                                wrapper = Tchae::NilWrapper, &proc)
      if block_given?
        tea_methodsymb = "__tea_#{methodname}".to_sym
        define_singleton_method "__tea_#{methodname}", proc
        define_singleton_method(methodname) do |*params, **kwparms|
          Tchae.do_validation_return_wrapper(self, wrapper,
                                        tea_methodsymb, proc,
                                        params, kwparms)
        end
      else
        define_singleton_method(methodname) {}
      end
    end
  end
end

# central return wrapper
def Return(result, error = nil)
  Tchae::ResultWrapper.new(result, error)
end

class Object
  extend Tchae::PI
  include Tchae::PISingleton
end

class Module
  include Tchae::PI
  include Tchae::PISingleton
end
