
## Tchae : a lightweight ruby validation library 

### Introduction 

Having to validate method parameters and/or the result of method is a 
quite common and repetitive task. 
**Tchae** is an atempt to capture this task in a small yet generic reuseable library. 
We provide **validated** variants of the standard *define_method* ruby methods , that will create the same method as the standard would create wrapped into validation code.

These methods are:
* **create_validated_method**   ... like *define_method* + validation
* **create_validated_singleton_method**   ... like *define_singleton_method* + validation

These methods have mostly the same arguments as their standard counterpart, and work similarly, excepted that they additionally can do validation. 

For the definition of constraints and rules we provide a class *MethodValidator*. 

When a validation fails, and we are in an error situation, **Tchae** can react in three ways
* either raise a exception,
* or return from the method without exception, and
  + return a wrapped *error object* . This kind of handling implies that valid result is returned in a wrapper as well.
  + return an *error object* but without using any wrapper. This kind of handling implies that valid result is returned directly without warpper in the valid case.

This error handling differentiation is defined on method validator level ( attribute *MethodValidator#handling* )  .

Validations typically make use of standard methods *is_a?* and/or *respond_to?* and we provide shortcuts to easily describe such rules.

**to be continued...** 

### Example

```ruby

require 'tchae'

class Something < String

  @validator = Tchae.Wrapper.args.positional.expect do 
    arg { is_a? Integer }
  end
  
  create_validated_method(:repeat, @validator) do |count|
    result = String.new
    count.times { result << self } 
    result 
  end
end

s = Something.new('Hello')
s.repeat(5)  # ==> "HelloHelloHelloHelloHello" 

s.repeat('five')  # ==> Tchae::ArgumentTypeException

```